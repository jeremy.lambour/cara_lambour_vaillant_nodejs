//Module base de données
var sqlite = require("sqlite3");
var db = new sqlite.Database("./db/db-cara-nodejs.db3");

/* Users */
function insertUser(nom, password) {
  db.run("insert into users(nom,password) values(?,?)", [nom, password]);
}

function selectAllUsers(cb) {
  return db.all("SELECT * FROM users", cb);
}

//Export fonctions
exports.insertUser = insertUser;
exports.selectAllUsers = selectAllUsers;
