var db = require("../db/db-module-nodejs");
var http = require("http");
var parse = require("querystring");
var url = require("url");

var server = http.createServer(function(req, res) {
  var page = url.parse(req.url).pathname;
  res.writeHead(200, { "Content-Type": "text/html" });
  if (page == "/login") {
    var pageContent =
      '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">' +
      '<div class="align-middle mx-auto w-50 p-3 text-center">' +
      '<form methode="get" action="/" >' +
      '<div class="card-header">Connexion</div>' +
      '<div class="card-body">' +
      '<div class="input-group mb-3">' +
      '<input type="text" class="form-control" placeholder="nom d\'utilisateur" aria-label="username"/>' +
      "</div>" +
      '<div class="input-group mb-3">' +
      '<input type="password" class="form-control" placeholder="mot de passe" aria-label="password"/>' +
      "</div>" +
      '<button type="button" class="btn btn-link">Vous avez oublie votre mot de passe ?</button>' +
      '<div class="justify-content-center">' +
      '<button type="submit" class="btn btn-primary">Connexion</button>' +
      "</div>" +
      "</div>" +
      "</form>" +
      "</div>";

    res.write(pageContent);
    res.end();
  } else if (page == "/register") {
    var pageContent =
      '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">' +
      '<div class="align-middle mx-auto w-50 p-3 text-center">' +
      '<form action="/registered"  methode="post">' +
      '<div class="card-header">Creation de compte</div>' +
      '<div class="card-body">' +
      '<div class="input-group mb-3">' +
      '<input type="text" class="form-control" placeholder="nom d\'utilisateur"  name="username" aria-label="username"/>' +
      "</div>" +
      '<div class="input-group mb-3">' +
      '<input type="password" class="form-control" placeholder="mot de passe"  name="password" aria-label="password"/>' +
      "</div>" +
      '<div class="justify-content-center">' +
      '<button type="submit" class="btn btn-primary">Connexion</button>' +
      "</div>" +
      "</div>" +
      "</form>" +
      "</div>";
    res.write(pageContent);
    res.end();
  } else if (page == "/registered") {
    var params = parse.parse(url.parse(req.url).query);
    console.log("params", params);
    console.log("username", params["username"]);
    console.log("password", params["password"]);
    db.insertUser(params["username"], params["password"]);
    res.writeHead(301, { Location: "/login" });
    res.end();
  } else if (page == "/") {
    let callback = (err, rows) => {
      if (err) {
        console.log(err);
      } else {
        rows = rows.filter(x => {
          if (x.nom !== null) {
            return x;
          }
        });
        res.writeHead(200, { "Content-Type": "application/json" });
        console.log(rows);
        res.write(JSON.stringify(rows));
        res.end();
      }
    };
    db.selectAllUsers(callback);
  } else {
    var pageContent =
      '<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">' +
      '<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>' +
      '<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>' +
      '<div class="container">' +
      '<div class="row">' +
      '<div class="col-md-12">' +
      '<div class="error-template">' +
      "<h1> Oops!</h1>" +
      "<h2> 404 Not Found</h2>" +
      '<div class="error-details">Cette page n\'existe pas</div>' +
      '<div class="error-actions">' +
      '<a href="http://localhost:8080" class="btn btn-default btn-lg">Retour a la page daccueil </a>' +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>";
    res.write(pageContent);
    res.end();
  }
});
server.listen(8080);
